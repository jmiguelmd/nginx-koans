# nginx koans

## A life of [web] service starts with:

1. `./run.sh`
2. Meditate on the test in `tests.rb`, and on `nginx/nginx.conf`

---

## Development

### Inspiration

https://github.com/edgecase/ruby_koans

### Resources

https://ieftimov.com/posts/how-to-write-rspec-formatters-from-scratch/