require 'net/http'
require 'uri'
require 'rspec'

describe 'A life of [web] service: ' do
  it "if at first you don't succeed" do
    uri = URI.parse('http://nginx:80/success')

    response = Net::HTTP.get_response(uri)

    expect(response.code).to eq('200')
    expect(response.body).to eq('Hello, world!')
  end

  it 'who cares' do
    uri = URI.parse('http://nginx:80/failure')

    response = Net::HTTP.get_response(uri)

    expect(response.code).to eq('400')
    expect(response.body).to eq('Hello, ???!')
  end

  it "we're a perfect match" do
    uri = URI.parse('http://nginx:80/')

    response = Net::HTTP.get_response(uri)

    expect(response.body).to eq('A')
  end

  it "we match pretty well" do
    uri = URI.parse('http://nginx:80/index.html')

    response = Net::HTTP.get_response(uri)

    expect(response.body).to eq('B')
  end

  it "we match even better" do
    uri = URI.parse('http://nginx:80/documents/document.html')

    response = Net::HTTP.get_response(uri)

    expect(response.body).to eq('C')
  end

  it "where are u at?" do
    log_file = "./nginx-logs/location.log"
    uri = URI.parse('http://nginx:80/location')

    response = Net::HTTP.get_response(uri)

    data = File.open(log_file).read.chomp
    expect(data).to eq('neither here nor there')
  end
end

