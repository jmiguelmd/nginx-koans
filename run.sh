#!/bin/bash

rm -rf nginx-logs/

docker compose up -d nginx > /dev/null 2>&1
./docker-compose-run.sh ruby 2>/dev/null
#./docker-compose-run.sh debug
#./docker-compose-run.sh solution
docker compose down -v > /dev/null 2>&1
