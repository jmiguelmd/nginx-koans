class NoDiffFormatter
RSpec::Core::Formatters.register self, :dump_summary
  def initialize(parameter_list)
  end

  def dump_summary notification
    total = notification.example_count;
    failed = notification.failure_count;

    if failed > 0
      success = total - failed;
      first_failed = notification.failed_examples.first;

      print "You're not there yet [#{success}/#{total}]\n\n"
      print "Meditate on the following test:\n\n"
      print "#{first_failed.full_description}\n\n"
    else
      print "Well done\n\n"
      print "You are ready to [web] serve\n"
    end

  end
end
