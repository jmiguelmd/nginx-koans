require 'net/http'
require 'uri'
require 'rspec'

describe 'A life of [web] service: ' do
  it "if at first you don't succeed" do
    uri = URI.parse('http://nginx:80/success')

    response = Net::HTTP.get_response(uri)

    expect(response.code).to eq('something')
    expect(response.body).to eq('something')
  end

  it 'who cares' do
    uri = URI.parse('http://nginx:80/failure')

    response = Net::HTTP.get_response(uri)

    expect(response.code).to eq('something')
    expect(response.body).to eq('something')
  end

  it "we're a perfect match" do
    uri = URI.parse('http://nginx:80/')

    response = Net::HTTP.get_response(uri)

    expect(response.body).to eq('something')
  end

  it "we match pretty well" do
    uri = URI.parse('http://nginx:80/index.html')

    response = Net::HTTP.get_response(uri)

    expect(response.body).to eq('something')
  end

  it "we match even better" do
    uri = URI.parse('http://nginx:80/documents/document.html')

    response = Net::HTTP.get_response(uri)

    expect(response.body).to eq('something')
  end

  it "where are u at?" do
    log_file = "./nginx-logs/location.log"
    uri = URI.parse('http://nginx:80/location')

    response = Net::HTTP.get_response(uri)

    data = File.open(log_file).read.chomp
    expect(data).to eq('something')
  end
end

